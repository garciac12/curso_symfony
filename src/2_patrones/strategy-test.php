<?php
use Curso\Persona\EstrategiaPersona;
use Curso\Persona\Persona;

function __autoload($className)  {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
    if (!file_exists($file)) {
        return false;
    }
    else {
        require $file;
        return true;
    }
}


$persona = new Persona();
$persona
    ->setNombre('Martin')
    ->setApellido('Garcia')
    ->setLegajo(12)
    ->setDocumento('50123123');

$default = new EstrategiaPersona(EstrategiaPersona::ESTRATEGIA_DEFAULT);
$full = new EstrategiaPersona(EstrategiaPersona::ESTRATEGIA_FULL);


echo "Default: " . $default->mostrarPersona($persona)."\n";
echo "Full: " . $full->mostrarPersona($persona)."\n";
