<?php
function __autoload($className)  {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
    if (!file_exists($file)) {
        return false;
    }
    else {
        require $file;
        return true;
    }
}

use Curso\Images\ImageProxy;
use Curso\Images\RawImage;

$path = './data/imagen.jpg';
$client = new \Curso\Images\ImageTag();

$image = new RawImage($path); // loading of the BLOB takes place
echo $client->tag($image), "\n";

$proxy = new ImageProxy($path);
echo $client->tag($proxy), "\n"; // loading does not take place even here

