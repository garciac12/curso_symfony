<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 25/10/16
 * Time: 00:15
 */

namespace Curso\Persona;


class EstrategiaPersona
{
    private $estrategia = NULL;
    const ESTRATEGIA_DEFAULT = 1;
    const ESTRATEGIA_FULL = 2;

    public function __construct($tipo) {
        if($tipo == EstrategiaPersona::ESTRATEGIA_DEFAULT ) {
            $this->estrategia = new PersonaDefault();
        } else if($tipo == EstrategiaPersona::ESTRATEGIA_FULL) {
            $this->estrategia = new PersonaFull();
        }
    }

    public function mostrarPersona(Persona $persona) {
        return $this->estrategia->mostrarPersona($persona);
    }
}