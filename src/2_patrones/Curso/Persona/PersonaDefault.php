<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 25/10/16
 * Time: 00:19
 */

namespace Curso\Persona;


class PersonaDefault implements EstrategiaInterface
{

    public function mostrarPersona(Persona $persona)
    {
        return $persona->getApellido() . ", " . $persona->getNombre();
    }
}