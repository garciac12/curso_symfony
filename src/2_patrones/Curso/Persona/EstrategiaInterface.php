<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 25/10/16
 * Time: 00:18
 */

namespace Curso\Persona;


interface EstrategiaInterface
{
    public function mostrarPersona(Persona $persona);
}