<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 25/10/16
 * Time: 00:19
 */

namespace Curso\Persona;


class PersonaFull implements EstrategiaInterface
{

    public function mostrarPersona(Persona $persona)
    {
        return $persona->getLegajo() . " - " . $persona->getApellido() . ", " . $persona->getNombre() . " - " . $persona->getDocumento();
    }
}