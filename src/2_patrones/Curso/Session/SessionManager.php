<?php

namespace Curso\Session;;

/*
 * Singleton para el manejo de una Session web.
 **/
class SessionManager implements SessionInterface {
    private static $instance;

    private $session_id;
    private $values = [];

    public static function getInstance()
    {
        if(self::$instance===null) {
            self::$instance = new SessionManager();
        }

        return self::$instance;
    }

    public function start()
    {
        $this->session_id = rand(0,10000);

        return $this;
    }

    public function close()
    {
        unset($this->session_id);
        $this->values = [];
    }

    public function get($name,$defaultValue=null)
    {
        if(array_key_exists($name, $this->values)!==false) {
            return $this->values[$name];
        }
        return $defaultValue;

    }

    public function set($name,$value)
    {
        $this->values[$name] = $value;

        return $this;
    }

    public function getId()
    {
        return $this->session_id;
    }
}
