<?php

namespace Curso\Session;

interface SessionInterface {
    public function start();
    public function close();
    public function get($name,$defaultValue=null);
    public function set($name,$value);
    public function getId();
}
