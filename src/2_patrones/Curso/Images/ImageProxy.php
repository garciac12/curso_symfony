<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 22:46
 */

namespace Curso\Images;


class ImageProxy extends AbstractImage
{
    public function __construct($path)
    {
        $this->_path = $path;
        list ($this->_width, $this->_height) = getimagesize($path);
    }

    /**
     * Creates a RawImage and exploits its functionalities.
     */
    protected function _lazyLoad()
    {
        if ($this->_realImage === null) {
            $this->_realImage = new RawImage($this->_path);
        }
    }

    public function dump()
    {
        $this->_lazyLoad();
        return $this->_realImage->dump();
    }
}