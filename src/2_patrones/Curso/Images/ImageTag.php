<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 22:48
 */

namespace Curso\Images;


class ImageTag
{
    public function tag(ImageInterface $img)
    {
        return '<img src="' . $img->getPath() . '" alt="" width="'
        . $img->getWidth() . '" height="'
        . $img->getHeight() . '" />';
    }
}
