<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 22:46
 */

namespace Curso\Images;


abstract class AbstractImage implements ImageInterface
{
    protected $_width;
    protected $_height;
    protected $_path;
    protected $_data;

    public function getWidth()
    {
        return $this->_width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function getPath()
    {
        return $this->_path;
    }
}
