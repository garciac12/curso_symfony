<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 22:45
 */

namespace Curso\Images;


interface ImageInterface
{
    public function getWidth();

    public function getHeight();

    public function getPath();

    public function dump();
}
