<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 22:46
 */

namespace Curso\Images;

class RawImage extends AbstractImage
{
    public function __construct($path)
    {
        $this->_path = $path;
        list ($this->_width, $this->_height) = getimagesize($path);
        $this->_data = file_get_contents($path);
    }

    public function dump()
    {
        return $this->_data;
    }
}