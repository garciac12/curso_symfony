<?php

namespace Curso\Grilla;

abstract class AbstractGrillaDirector {
    abstract function __construct(AbstractGrillaBuilder $builder);
    abstract function getGrilla();
}
