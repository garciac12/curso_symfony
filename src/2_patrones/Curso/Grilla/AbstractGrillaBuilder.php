<?php

namespace Curso\Grilla;

abstract class AbstractGrillaBuilder {
    protected $titulo;
    protected $columnas;
    protected $data;

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setColumnas($columnas)
    {
        $this->columnas = $columnas;

        return $this;
    }

    public function getColumnas()
    {
        return $this->columnas;
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public abstract function render();


}
