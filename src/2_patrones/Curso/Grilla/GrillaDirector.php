<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 26/10/16
 * Time: 00:12
 */

namespace Curso\Grilla;


class GrillaDirector extends AbstractGrillaDirector
{
    protected $builder;

    public function __construct(AbstractGrillaBuilder $builder)
    {
        $this->builder = $builder;
    }

    function getGrilla()
    {
        return $this->builder->render();
    }
}