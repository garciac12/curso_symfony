<?php

namespace Curso\Grilla;

class GrillaFactory {

    public static function crearGrilla($class)
    {
        	if (class_exists($class) ) {
                return new $class();
            }
    }
}
