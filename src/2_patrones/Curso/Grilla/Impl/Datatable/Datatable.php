<?php

namespace Curso\Grilla\Impl\Datatable;

use Curso\Grilla\AbstractGrillaBuilder;

class Datatable extends AbstractGrillaBuilder  {
    public function render()
    {
        $html =  "<table class='datatable'>";
        $html .= "<thead>";
        foreach($this->columnas as $key=>$columna) {
            $html .= "<th>" . $columna . "</th>";
        }
        $html .= "</tbody>";
        $html .= "</table>";

        $html .= "<script>";
        $html .= '$(document).ready(){$(".datatable").datatable({});}';
        $html .= "</script>";
        return $html;
    }
}
