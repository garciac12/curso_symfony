<?php

namespace Curso\Grilla\Impl\Html;

use \Curso\Grilla\AbstractGrillaBuilder;

class HtmlTable extends AbstractGrillaBuilder {
    public function render()
    {
        $html =  "<table>";
        $html .= "<thead>";
        foreach($this->columnas as $key=>$columna) {
            $html .= "<th>" . $columna . "</th>";
        }
        $html .= "</tbody>";
        $html .= "</table>";

        return $html;
    }
}
