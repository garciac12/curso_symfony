<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 23:58
 */

namespace Curso\Filtro;


class TipoFiltro extends AbstractFiltro
{

    public function crearFiltro(&$query, $params)
    {
        $query[] = ' AND tipo = ' . $params['tipo'];

        if($this->sucesor) {
            $this->sucesor->crearFiltro($query, $params);
        }

        return $query;
    }
}