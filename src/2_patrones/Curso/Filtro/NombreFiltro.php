<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 23:58
 */

namespace Curso\Filtro;


class NombreFiltro extends AbstractFiltro
{

    public function crearFiltro(&$query, $params)
    {
        $query[] = ' AND nombre = ' . $params['nombre'];

        if($this->sucesor) {
            $this->sucesor->crearFiltro($query, $params);
        }

        return $query;
    }
}