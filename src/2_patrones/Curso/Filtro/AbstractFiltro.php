<?php
/**
 * Created by PhpStorm.
 * User: valavalla
 * Date: 24/10/16
 * Time: 23:56
 */

namespace Curso\Filtro;


abstract class AbstractFiltro
{
    protected $sucesor;

    public function setSucesor(AbstractFiltro $sucesor)
    {
        $this->sucesor = $sucesor;
        return $this;
    }

    public function getSucesor()
    {
        return $this->sucesor;
    }

    abstract function crearFiltro(&$query, $params);
}