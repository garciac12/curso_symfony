<?php
function __autoload($className)  {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
    if (!file_exists($file)) {
        return false;
    }
    else {
        require $file;
        return true;
    }
}

use Curso\Filtro\FechaFiltro;
use Curso\Filtro\NombreFiltro;
use Curso\Filtro\TipoFiltro;

$fechaFiltro = new FechaFiltro();
$nombreFiltro = new NombreFiltro();
$tipoFiltro = new TipoFiltro();

$fechaFiltro->setSucesor($nombreFiltro);
//s$nombreFiltro->setSucesor($tipoFiltro);

$query = $fechaFiltro->crearFiltro($query, ['nombre'=>'Carlos', 'tipo'=>'1', 'fecha'=>'2016-10-22']);

var_dump($query);
